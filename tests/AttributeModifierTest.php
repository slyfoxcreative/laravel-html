<?php

declare(strict_types=1);

namespace SlyFoxCreative\Html\Tests;

use Illuminate\Support\HtmlString;
use SlyFoxCreative\Html\AttributeModifierRegistry;

use function SlyFoxCreative\Html\select;
use function SlyFoxCreative\Html\text;

class AttributeModifierTest extends TestCase
{
    public function testModifier(): void
    {
        resolve(AttributeModifierRegistry::class)->add(function ($attributes, $context) {
            $attributes['class'] = 'test';

            return $attributes;
        });

        self::assertEquals(
            new HtmlString("<input class='test' id='test' name='test' type='text'>"),
            text('test'),
        );
    }

    public function testTaggedModifier(): void
    {
        $callable = function ($attributes, $context) {
            $attributes['class'] = 'test';

            return $attributes;
        };

        resolve(AttributeModifierRegistry::class)->add($callable, ['input']);

        self::assertEquals(
            new HtmlString("<input class='test' id='test' name='test' type='text'>"),
            text('test'),
        );

        self::assertEquals(
            new HtmlString("<select id='test' name='test'>\n</select>"),
            select('test', []),
        );
    }

    public function testContext(): void
    {
        resolve(AttributeModifierRegistry::class)->add(function ($attributes, $context) {
            $attributes['name'] = mb_strtoupper($context['name']);

            return $attributes;
        });

        self::assertEquals(
            new HtmlString("<input id='test' name='TEST' type='text'>"),
            text('test'),
        );
    }

    public function testContextWithNestedName(): void
    {
        resolve(AttributeModifierRegistry::class)->add(function ($attributes, $context) {
            $attributes['data-name'] = $context['name'];

            return $attributes;
        });

        self::assertEquals(
            new HtmlString("<input data-name='test.nested' id='test_nested' name='test[nested]' type='text'>"),
            text('test[nested]'),
        );
    }
}
