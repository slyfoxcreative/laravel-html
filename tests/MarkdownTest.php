<?php

declare(strict_types=1);

namespace SlyFoxCreative\Html\Tests;

use Illuminate\Support\HtmlString;

use function SlyFoxCreative\Html\markdown;

class MarkdownTest extends TestCase
{
    public function testMarkdown(): void
    {
        $text = 'This is markdown.';

        $expected = new HtmlString("<p>This is markdown.</p>\n");

        self::assertEquals($expected, markdown($text));
    }

    public function testStripsHtml(): void
    {
        $text = 'This is <i>markdown</i>.';

        $expected = new HtmlString("<p>This is markdown.</p>\n");

        self::assertEquals($expected, markdown($text));
    }

    public function testDisallowsUnsafeLinks(): void
    {
        $text = 'This is [markdown](file:///markdown.html).';

        $expected = new HtmlString("<p>This is <a>markdown</a>.</p>\n");

        self::assertEquals($expected, markdown($text));
    }
}
