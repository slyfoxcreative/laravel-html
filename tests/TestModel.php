<?php

declare(strict_types=1);

namespace SlyFoxCreative\Html\Tests;

use Illuminate\Database\Eloquent\Model;

class TestModel extends Model
{
    protected $fillable = [
        'test',
    ];
}
