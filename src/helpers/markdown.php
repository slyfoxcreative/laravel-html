<?php

declare(strict_types=1);

namespace SlyFoxCreative\Html;

use Illuminate\Support\HtmlString;
use League\CommonMark\CommonMarkConverter;

function markdown(string $text): HtmlString
{
    return new HtmlString(
        (string) resolve(CommonMarkConverter::class)->convert($text),
    );
}
