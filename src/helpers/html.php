<?php

declare(strict_types=1);

namespace SlyFoxCreative\Html;

use Illuminate\Support\Collection;
use Illuminate\Support\HtmlString;

use function SlyFoxCreative\Html\Helpers\attributeString;

/** @param AttributeList $attributes */
function opening_tag(string $name, array|Collection $attributes = []): HtmlString
{
    $attributes = is_array($attributes) ? collect($attributes) : $attributes;

    $attributeString = attributeString($attributes);
    if ($attributeString !== '') {
        $attributeString = " {$attributeString}";
    }

    return new HtmlString("<{$name}{$attributeString}>");
}

function closing_tag(string $name): HtmlString
{
    return new HtmlString("</{$name}>");
}

/** @param AttributeList $attributes */
function complete_tag(string $name, string $content, array|Collection $attributes = []): HtmlString
{
    $attributes = is_array($attributes) ? collect($attributes) : $attributes;

    return new HtmlString(opening_tag($name, $attributes) . $content . closing_tag($name));
}
