<?php

declare(strict_types=1);

namespace SlyFoxCreative\Html;

use Illuminate\Support\Collection;

/**
 * An attribute modifier stored in the registry.
 */
class AttributeModifier
{
    /**
     * The callable that takes a Collection of attributes and an optional array
     * of context values and returns a modified Collection.
     */
    private \Closure $modifier;

    /**
     * The Collection of tags that indicate when the modifier is to be applied.
     * A modifier with an empty tag Collection applies to all tags.
     *
     * @var Collection<int, string>
     */
    private Collection $tags;

    /** @param array<int, string> $tags */
    public function __construct(callable $modifier, array $tags = [])
    {
        $this->modifier = $modifier(...);
        $this->tags = collect($tags);
    }

    /**
     * Return true or false whether the modifier applies to the given tag.
     */
    public function appliesToTag(string $tag): bool
    {
        return $this->tags->isEmpty() || $this->tags->contains($tag);
    }

    /**
     * Modify the given Collection of attributes and return it.
     *
     * @param  AttributeCollection  $attributes
     * @param  array<string, mixed>  $context
     * @return AttributeCollection
     */
    public function apply(Collection $attributes, array $context): Collection
    {
        return call_user_func($this->modifier, $attributes, $context);
    }
}
