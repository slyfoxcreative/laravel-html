<?php

declare(strict_types=1);

namespace SlyFoxCreative\Html;

use Illuminate\Support\Collection;

/**
 * A stack of objects. Used in Blade template rendering for the stack of
 * form-model objects specified in the form component's model attribute and
 * the @model directive.
 */
class FormModelStack
{
    /** @var Collection<int, object> */
    private Collection $stack;

    public function __construct()
    {
        $this->stack = new Collection();
    }

    public function push(object $model): void
    {
        $this->stack->push($model);
    }

    public function pop(): void
    {
        $this->stack->pop();
    }

    public function last(): ?object
    {
        return $this->stack->last();
    }
}
